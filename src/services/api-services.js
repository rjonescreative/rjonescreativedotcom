const BASE_API = 'https://api.scryfall.com';
const API = {
  CARD_COLLECTION: `${BASE_API}/cards/collection`,
  CARD_NAMED: `${BASE_API}/cards/named`,
  CARD_SEARCH: `${BASE_API}/cards/search`,
};

function handleErrors(res) {
  if (!res.ok) {
    throw Error(res.status);
  }
  return res;
}

function setHeaders() {
  return new Headers({
    'Content-Type': 'application/json',
  });
}

/* ** API Calls ** */

function getNamedCard(query) {
  return fetch(`${API.CARD_NAMED}${query}`, {
    method: 'GET',
  })
    .then(handleErrors)
    .then(res => res.json())
    .catch(err => err);
}

function getCardCollection(collection) {
  return fetch(API.CARD_COLLECTION, {
    method: 'POST',
    headers: setHeaders(),
    body: JSON.stringify(collection),
  })
    .then(handleErrors)
    .then(res => res.json())
    .catch(err => err);
}

export default {
  getNamedCard,
  getCardCollection,
};
